﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ReadableSource
{
    class Program
    {
        static void Main(string [] args)
        {
            string dir = args[0];
            string outputPath = args[1];
            string pattern = (args.Length > 2) ? args[2] : "*";
            StringBuilder b = new StringBuilder();
            foreach (string file in Directory.GetFiles(dir, pattern, SearchOption.AllDirectories))
            {
                b.Append(file);
                b.Append(Environment.NewLine);
                b.Append(File.ReadAllText(file));
                b.Append(Environment.NewLine);
            }

            File.WriteAllText(outputPath, b.ToString());
        }
    }
}
